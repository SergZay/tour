<?php
/**
 * Plugin Name: Tour
 * Version: 1.0
 * Description: Tour plugin!
 * Author: SergZay
 */

require_once __DIR__ . '/includes/StyleScript.php';
require_once __DIR__ . '/includes/TourPostType.php';
require_once __DIR__ . '/includes/CountryTaxonomy.php';
require_once __DIR__ . '/includes/metabox/Gallery.php';
require_once __DIR__ . '/includes/metabox/Date.php';
require_once __DIR__ . '/includes/metabox/TextInput.php';

if ( ! class_exists( 'Tour' ) ) {
	class Tour {

		public function __construct() {
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
			register_uninstall_hook( __FILE__, array( $this, 'uninstall' ) );

			$event_start_date = new Date( 'tour', 'tour_start_date', 'Tour start date', "Start Date" );
			$event_end_date   = new Date( 'tour', 'tour_end_date', 'Tour end date', "End Date" );
		}

		public static function activate() {

		}

		public static function deactivate() {
			flush_rewrite_rules();
		}

		public static function uninstall() {

		}

	}


	new Tour();
}