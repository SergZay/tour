<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();

	?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <header class="entry-header alignwide">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php the_post_thumbnail(); ?>
        </header>

        <div class="entry-content">
			<?php the_content(); ?>
			<?php
			$image_ids = get_post_meta( get_the_ID(), 'second_featured_img' );

			if ( ! empty( $image_ids ) ) :
				$image_ids = explode( ',', $image_ids[0] );
				foreach ( $image_ids as $image_id ) {
					$image_url = wp_get_attachment_url( $image_id ); ?>
                    <div class="featured-item">
                        <img src="<?php echo $image_url; ?>"/>
                    </div>
					<?php
				}
			endif;
			?>
			<?php
			$price     = get_post_meta( get_the_ID(), 'price-input' );
			$startDate = get_post_meta( get_the_ID(), 'tour_start_date' );
			$endDate   = get_post_meta( get_the_ID(), 'tour_end_date' );
			?>
            <p>Country:</p>
			<?php
			$cur_terms = get_the_terms( get_the_ID(), 'country' );
			if ( is_array( $cur_terms ) ) {
				foreach ( $cur_terms as $cur_term ) {
					echo '<li>' . $cur_term->name . '</li>';
				}
			} ?>
            <p>Price: <?php echo $price[0]; ?></p>
            <p>Start Date: <?php echo $startDate[0]; ?></p>
            <p>End Date: <?php echo $endDate[0]; ?></p>
        </div><!-- .entry-content -->

    </article><!-- #post-<?php the_ID(); ?> -->
<?php

endwhile; // End of the loop.

get_footer();
