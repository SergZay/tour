<?php
class Date {

	var $post_type;
	var $custom_field_name;
	var $metabox_title;
	var $custom_field_label;

	function __construct( $post_type = 'post', $custom_field_name = "_datepicker", $custom_field_label = "Datepicker", $metabox_title = "The Datepicker" ) {
		$this->post_type          = $post_type;
		$this->custom_field_name  = $custom_field_name;
		$this->custom_field_label = $custom_field_label;
		$this->metabox_title      = $metabox_title;

		add_action( 'add_meta_boxes', array( &$this, 'register_datepicker_metabox' ) );
		add_action( 'save_post', array( &$this, 'save_date' ) );
	}

	function register_datepicker_metabox() {
		add_meta_box(
			$this->metabox_title . '_datepicker_section',
			$this->metabox_title,
			array( &$this, 'datepicker_metabox_content' ),
			$this->post_type,
			'side',
			'core'
		);

	}

	function datepicker_metabox_content( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), $this->custom_field_name . '_datepicker_nonce' );

		echo "<label for='" . $this->custom_field_name . "'>" . $this->custom_field_label . "</label> ";
		echo "<input class='datepicker' type='text' name='" . $this->custom_field_name . "' value='" . get_post_meta( $post->ID, $this->custom_field_name, true ) . "' />";
	}

	function save_date( $post_id ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST[ $this->custom_field_name . '_datepicker_nonce' ] ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_POST[ $this->custom_field_name . '_datepicker_nonce' ], plugin_basename( __FILE__ ) ) ) {
			return;
		}

		if ( 'page' == $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}

		update_post_meta( $post_id, $this->custom_field_name, $_POST[ $this->custom_field_name ], get_post_meta( $post_id, $this->custom_field_name, true ) );
	}

}