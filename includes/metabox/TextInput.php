<?php

class TextInput {

	public function __construct() {

		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_meta_box_data' ) );

	}

	/**
	 * Adds a meta box to the post editing screen
	 */
	public function add_meta_box() {

		add_meta_box(
			'custom_meta_box',
			'Price',
			array( $this, 'display_custom_meta_box' ),
			'tour',
			'side',
			'low'
		);

	}

	public function display_custom_meta_box() {

		$html = '';

		wp_nonce_field( 'nonce_check', 'nonce_check_value' );

		$html .= '<input type="text" name="price-input" id="price-input" value="' . get_post_meta( get_the_ID(), 'price-input', true ) . '" placeholder="Enter your price" />';

		echo $html;
	}

	public function save_meta_box_data( $post_id ) {

		var_dump( $post_id );

		if ( $this->user_can_save( $post_id, 'nonce_check_value' ) ) {

			// Checks for input and sanitizes/saves if needed
			if ( isset( $_POST['price-input'] ) && 0 < count( strlen( trim( $_POST['price-input'] ) ) ) ) {

				update_post_meta( $post_id, 'price-input', sanitize_text_field( $_POST['price-input'] ) );

			}

		}

	}

	public function user_can_save( $post_id, $nonce ) {

		// Checks save status
		$is_autosave    = wp_is_post_autosave( $post_id );
		$is_revision    = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ $nonce ] ) && wp_verify_nonce( $_POST[ $nonce ], 'nonce_check' ) ) ? 'true' : 'false';

		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {

			return false;

		}

		return true;

	}

}

new TextInput();