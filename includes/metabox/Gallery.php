<?php

class Gallery {

	function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'meta_box_add' ) );
		add_action( 'save_post', array( $this, 'img_gallery_save' ) );
	}

	function meta_box_add() {
		add_meta_box( 'feat_img_slider',
			'Featured Image Gallery',
			array( $this, 'print_box' ),
			'tour',
			'side',
			'default' );
	}

	function print_box( $post ) {

		wp_nonce_field( 'save_feat_gallery', 'feat_gallery_nonce' );

		$meta_key = 'second_featured_img';
		echo $this->image_uploader_field( $meta_key, get_post_meta( $post->ID, $meta_key, true ) );
	}

	function image_uploader_field( $name, $value = '' ) {

		$image      = 'Upload Image';
		$button     = 'button';
		$image_size = 'full';
		$display    = 'none';

		?>

        <p><?php
			_e( '<i>Set Images for Featured Image Gallery</i>', 'mytheme' );
			?></p>

        <label>
            <div class="gallery-screenshot clearfix">
				<?php
				{
					$ids = explode( ',', $value );
					foreach ( $ids as $attachment_id ) {
						$img = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
						echo '<div class="screen-thumb"><img src="' . esc_url( $img[0] ) . '" /></div>';
					}
				}
				?>
            </div>

            <input id="edit-gallery" class="button upload_gallery_button" type="button"
                   value="<?php esc_html_e( 'Add/Edit Gallery', 'mytheme' ) ?>"/>
            <input id="clear-gallery" class="button upload_gallery_button" type="button"
                   value="<?php esc_html_e( 'Clear', 'mytheme' ) ?>"/>
            <input type="hidden" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $name ); ?>"
                   class="gallery_values" value="<?php echo esc_attr( $value ); ?>">
        </label>
		<?php
	}

	function img_gallery_save( $post_id ) {

		if ( ! isset( $_POST['feat_gallery_nonce'] ) ) {
			return $post_id;
		}

		if ( ! wp_verify_nonce( $_POST['feat_gallery_nonce'], 'save_feat_gallery' ) ) {
			return $post_id;
		}

		if ( isset( $_POST['second_featured_img'] ) ) {
			update_post_meta( $post_id, 'second_featured_img', esc_attr( $_POST['second_featured_img'] ) );
		} else {
			update_post_meta( $post_id, 'second_featured_img', '' );
		}

	}

}

new Gallery();