<?php

namespace Includes;

class StyleScript {

	function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'files' ) );
	}

	public function files() {
		wp_enqueue_script( 'gallery-js', plugins_url( 'js/gallery.js', __FILE__ ), array( 'jquery' ), null, true );
		wp_enqueue_script( 'datepicker', plugins_url( 'js/jquery-ui-1.8.20.custom.min.js', __FILE__ ), array( 'jquery' ), '', true );
		wp_enqueue_script( 'datepicker-options', plugins_url( 'js/call_datepicker.js', __FILE__ ), array( 'datepicker' ), '', true );

		wp_enqueue_style( 'datepickers', plugins_url( 'css/ui-lightness/jquery-ui-1.8.20.custom.css', __FILE__ ) );
	}

}

new StyleScript();