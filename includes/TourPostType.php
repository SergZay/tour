<?php

namespace PostType;

class TourPostType {

	private $type = 'tour';
	private $slug = 'tour';
	private $name = 'Tour';
	private $singular_name = 'Tour';


	public function register() {
		$labels = array(
			'name'               => $this->name,
			'singular_name'      => $this->singular_name,
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New ' . $this->singular_name,
			'edit_item'          => 'Edit ' . $this->singular_name,
			'new_item'           => 'New ' . $this->singular_name,
			'all_items'          => 'All ' . $this->name,
			'view_item'          => 'View ' . $this->name,
			'search_items'       => 'Search ' . $this->name,
			'not_found'          => 'No ' . strtolower( $this->name ) . ' found',
			'not_found_in_trash' => 'No ' . strtolower( $this->name ) . ' found in Trash',
			'parent_item_colon'  => '',
			'menu_name'          => $this->name
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $this->slug ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => 8,
			'supports'           => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail' ),
			'yarpp_support'      => true
		);

		register_post_type( $this->type, $args );
	}


	public function set_columns( $columns ) {

		unset( $columns['author'] );
		unset( $columns['date'] );

		$i = 0;
		foreach ( $columns as $col => $name ) {
			$i ++;
			switch ( $i ) {
				case 3:
					$out['description'] = 'Description';
					break;
			}
			$out[ $col ] = $name;
		}
		$out['tourdate'] = 'Date';
		$out['price']    = 'Price';

		return $out;
	}

	public function edit_columns( $column, $post_id ) {
		switch ( $column ) {
			case 'description':
				the_content();
				break;
			case 'tourdate':
				echo get_post_meta( $post_id, 'tour_start_date', 1 );
				echo " - ";
				echo get_post_meta( $post_id, 'tour_end_date', 1 );
				break;
			case 'price':
				echo get_post_meta( $post_id, 'price-input', 1 );
				break;
		}
	}

	function tour_template( $single ) {

		global $post;

		/* Checks for single template by post type */
		if ( $post->post_type == $this->type ) {
			if ( file_exists( __DIR__ . '/template/single.php' ) ) {
				return __DIR__ . '/template/single.php';
			}
		}

		return $single;

	}

	public function rewrite() {
		flush_rewrite_rules();
	}

	public function __construct() {

		add_action( 'init', array( $this, 'register' ) );

		add_action( 'init', array( $this, 'rewrite' ) );

		add_filter( 'manage_edit-' . $this->type . '_columns', array( $this, 'set_columns' ), 10, 1 );

		add_action( 'manage_' . $this->type . '_posts_custom_column', array( $this, 'edit_columns' ), 10, 2 );

		add_filter( 'single_template', array( $this, 'tour_template' ) );

	}

}

new TourPostType();